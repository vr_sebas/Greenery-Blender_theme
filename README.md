<h2>README</h2>

This is a theme built over the current blender theme: Modern minimalist.

Colors are based on Pitiwazou's theme.

I've made some changes to outlines, highlights and color hierachies to fit my 
personal taste.

Use it or modify it at your own, please give me feedback if there's way to 
improve it.